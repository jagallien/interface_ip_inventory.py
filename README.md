# interface_ip_inventory.py
#### Pulls interface name & IP address of every device-interface from SolarWinds, outputs as .csv (ip,interface_name.hostname.fqdn)

##### Requirements:
  - [Python 3.6]
  - NOTE: you must check the "add path variable" box during installation!
  - [orionsdk] (pip install orionsdk)
  - urllib3 (pip install urllib3)

[Python 3.6]: https://www.python.org/ftp/python/3.6.4/python-3.6.4.exe
[orionsdk]: https://github.com/solarwinds/OrionSDK


Usage: interface_ip_inventory.py <solarwinds_server> <username> <password>
