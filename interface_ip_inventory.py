#!/usr/bin/python3

###################
#
# interface_ip_inventory.py :: gets the interface name & IP address of every device-interface.
# by: J.A.Gallien, jagallien@protonmail.com
#
###################

from datetime import date # Used for naming our file w/ the current date.
import orionsdk
import urllib3
import sys
import re

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning) #removes "insecure" warnings from CLI output :\


output_file = open((date.today().strftime('%Y%m%d') + '_interface_ip_inventory.csv'), mode='w')
connector = orionsdk.SwisClient(sys.argv[1], sys.argv[2], sys.argv[3])


def interface_getter(long_node_id):
    #This is specifically used to gather interface names and indexes
    cirrus_interfaces_query = "SELECT TOP 200 NodeID, InterfaceID, InterfaceIndex, InterfaceName, InterfaceAlias, InterfaceType, InterfaceTypeName FROM Cirrus.Interfaces Where NodeID LIKE '{0}' and InterfaceTypeName NOT LIKE '%voice%' AND NodeID LIKE '{0}' and InterfaceTypeName NOT LIKE '%other%'" .format(long_node_id)
    cirrus_interfaces = connector.query(cirrus_interfaces_query)
    cirrus_interfaces = (cirrus_interfaces['results'])
    return cirrus_interfaces

def ip_getter(interfaceindex,short_node_id):
    #Used to return IP Address of a particular interface)
    node_ip_query = "SELECT TOP 1 NodeID, IPAddress, InterfaceIndex, SubnetMask FROM Orion.NodeIPAddresses WHERE NodeId LIKE '{0}' AND InterfaceIndex Like '{1}'" .format(short_node_id,interfaceindex)
    ip = connector.query(node_ip_query)
    ip = (ip['results'])
    if len(ip) <= 0:
        ip = 'NONE'
        return ip
    else:
        ip = str(ip[0]['IPAddress'])
        return ip

if len(sys.argv) < 4:
    print('\n\n\tUsage: interface_ip_inventory.py <solarwinds_server> <username> <password>\n\n')
else:
    #Get nodes, extract "NodeID"(the NodeID variable is used for something else within SWQL later)
    nodes_found = connector.query("SELECT TOP 500 NodeID, CoreNodeID, AgentIP, SysName, NodeComments FROM Cirrus.Nodes")
    nodes_found = (nodes_found['results'])
    for i in range(len(nodes_found)):
        short_node_id,long_node_id,sysname = (str(nodes_found[i]['CoreNodeID']) , nodes_found[i]['NodeID'] , nodes_found[i]['SysName'])
        interfaces = (interface_getter(long_node_id))

        for i in range(len(interfaces)):
            interfaceindex, interfacename = (str(interfaces[i]['InterfaceIndex']) , interfaces[i]['InterfaceName'])
            ip = ip_getter(interfaceindex,short_node_id)
            newinterfacename = re.sub('[\/ \.]', '-', interfacename)
            if ip == 'NONE':
                continue
            else:
                output_file.write((str(ip)) + ',' + newinterfacename + '.' + sysname + '\n')
                #print((str(ip)) + ',' + newinterfacename + '.' + sysname) #Debugger :)
